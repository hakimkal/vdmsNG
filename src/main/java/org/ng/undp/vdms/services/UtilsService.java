package org.ng.undp.vdms.services;

import org.ng.undp.vdms.configs.AppConfigProperties;
import org.ng.undp.vdms.domains.constants.Env;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




/**
 * Created by emmanuel on 7/6/17.
 */
@Service
public class UtilsService {

    @Autowired
    protected AppConfigProperties properties;

    /**
     * Get current application profile, possible values are : DEV, PROD, SCHOOL
     *
     * @return
     */
    public Env getEnvironmentProfile() {
        return Env.valueOf(properties.getActiveProfile().toUpperCase());
    }
}
