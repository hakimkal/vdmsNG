package org.ng.undp.vdms.services;

import org.ng.undp.vdms.constants.NgoDocumentType;
import org.ng.undp.vdms.constants.SupplierDocumentType;
import org.ng.undp.vdms.domains.NgoDocument;
import org.ng.undp.vdms.domains.SupplierDocument;
import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.repositories.NgoDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Author: Kingsley Eze.
 * Project: vdms
 * Date: 6/19/2017.
 */

@Service
public class NgoDocumentService {

    @Autowired
    private NgoDocumentRepository repository;

    public NgoDocument save(NgoDocument document){
        return repository.save(document);
    }

       public NgoDocument findOne(Long document){
        return repository.findOne(document);
    }

    public NgoDocument findOneByUserAndNgoDocumentType(User user, NgoDocumentType supplierDocumentType){
        return repository.findOneByUserAndNgoDocumentType(user,supplierDocumentType);
    }
}
