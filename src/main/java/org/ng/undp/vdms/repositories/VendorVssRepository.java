package org.ng.undp.vdms.repositories;


import org.ng.undp.vdms.domains.Vendor;
import org.ng.undp.vdms.domains.VendorVss;
import org.ng.undp.vdms.domains.Vss;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by abdulhakim on 7/16/17.
 */
public interface VendorVssRepository  extends CrudRepository<VendorVss, Long>, JpaSpecificationExecutor<VendorVss> {


    @Query("SELECT q FROM VendorVss q WHERE  q.id IN (:vpaIds)")
    public List<VendorVss> findAllById(@Param("vpaIds") List<Long> vpaIds);

    public VendorVss findOneByVendorAndVss(Vendor v, Vss id);

}
