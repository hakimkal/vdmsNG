package org.ng.undp.vdms.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.ng.undp.vdms.domains.SupplierMembership;

import java.util.List;

/**
 * Author: Kingsley Eze.
 * Project: vdms
 * Date: 6/19/2017.
 */

@Data
public class SupplierOtherDto {

    private Long id;

    private String supplierDispute1;

    private String supplierDispute2;

    private String supplierDispute3;

    @NotEmpty(message = "Please enter functional title")
    private String functionalTitle;

    @NotEmpty(message = "Please enter your full name")
    private String signature;

    private String supplierMembershipArray;

    private List<SupplierMembership> supplierMemberships;
}
