package org.ng.undp.vdms.schedules;

import org.ng.undp.vdms.domains.SupplierCompany;
import org.ng.undp.vdms.services.SupplierCompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by abdulhakim on 3/18/18.
 */
@Component

public class CheckDuplicateSupplierCompany {

    @Autowired
    SupplierCompanyService supplierCompanyService;
    private static Logger LOGGER = LoggerFactory.getLogger(CheckDuplicateSupplierCompany.class);

    @Scheduled(cron = "0 0 0 * * *", zone = "Africa/Lagos")
    public void checkSupplierCompany() {
        LOGGER.info("Background job for supplier company duplicate removal running");


        Iterable<SupplierCompany> supplierCompanyList = supplierCompanyService.findAll();

        for (SupplierCompany s : supplierCompanyList) {

            List<SupplierCompany> supplierCompanies = supplierCompanyService.findAllByUser(s.getUser());

            if (supplierCompanies.size() > 1) {

                supplierCompanies.remove(0);
                deleteSupplierCompany(supplierCompanies);

            }

        }


    }

    @Async
    public void deleteSupplierCompany(List<SupplierCompany> supplierCompanies) {
        for (SupplierCompany supplierCompany : supplierCompanies) {

            LOGGER.info("Deleting suppliercompany duplicate  :: " + supplierCompany.getName());
            supplierCompanyService.delete(supplierCompany.getId());

        }


    }
}
