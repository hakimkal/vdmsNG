package org.ng.undp.vdms.repositories;

import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.domains.security.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * Created by Samuel on 10/13/2016.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT q FROM  User q WHERE  q.userTypes IN (:userTypes)")
    public List<User> findAllByUserTypes(@Param("userTypes") String[] userTypes, Pageable pageable);

    @Query("SELECT q FROM  User q WHERE  q.deletedAt is NULL and  q.username like concat(:string, '%')  or q.firstname like concat(:string, '%') or q.email like concat(:string, '%')  and  q.deletedAt is NULL order by q.firstname DESC ")
    public List<org.ng.undp.vdms.domains.User> findAll(@Param("string") String string);

    @Query("SELECT q FROM  User q WHERE  q.userTypes IN (:userTypes)")
    public List<User> findAllByUserTypes(@Param("userTypes") String[] userTypes);

    @Query("SELECT q FROM  User q WHERE  q.deletedAt is NULL ORDER BY  q.created_at DESC")

    Page<User> findAllByDeletedAtIsNullOrderByCreatedAtDesc(Pageable pageable);

    @Query("SELECT q FROM  User q WHERE  q.deletedAt is NULL ORDER BY  q.created_at ASC")

    Page<User> findAllByDeletedAtIsNullOrderByCreatedAtAsc(Pageable pageable);

    @Query("SELECT q FROM  User q WHERE  q.deletedAt is NOT NULL")

    Page<User> findAllByDeletedAtIsNotNull(Pageable pageable);

    public List<User> findAllByRoles(Set<Role> userTypes, Pageable pageable);
    public List<User> findAllByRoles(Set<Role> userTypes);

    @Query("SELECT distinct  u FROM User u  JOIN u.roles r  WHERE r.name = 'STAFF'  AND   u.deletedAt is NULL")
    List<User> findAllStaff();
    @Query("SELECT distinct  u FROM User u  JOIN u.roles r WHERE r.name = 'ADMIN_ACCOUNT' OR r.name = 'STAFF'  AND   u.deletedAt is NULL")
    List<User> findAllStaffAndAdmin();

    @Query("SELECT distinct  u FROM User u  JOIN u.roles r WHERE  r.name = 'STAFF'  AND u.deletedAt is NULL")
    Page<User> findAllStaff(Pageable pageable);

    @Query("SELECT distinct  u FROM User u  JOIN u.roles r WHERE r.name = 'ADMIN_ACCOUNT'   AND   u.deletedAt is NULL")
    Page<User> findAllAdmin(Pageable pageable);
    @Query("SELECT distinct  u FROM User u  JOIN u.roles r WHERE r.name = 'ADMIN_ACCOUNT'   AND   u.deletedAt is NULL")
    List<User> findAllAdmin();

    @Query("SELECT distinct  u FROM User u  JOIN u.roles r WHERE r.name = 'ADMIN_ACCOUNT' OR r.name = 'STAFF'  AND   u.deletedAt is NULL")
    Page<User> findAllStaffAndAdmin(Pageable pageable);

    @Query("SELECT distinct  u FROM User u  JOIN u.roles r WHERE r.name = 'SUPPLIER'    AND   u.deletedAt is NULL")
    List<User> findAllSuppliers();

    @Query("SELECT distinct  u FROM User u  JOIN u.roles r WHERE r.name = 'NGO'    AND   u.deletedAt is NULL")
    List<User> findAllNGOs();
    @Query("SELECT distinct  u FROM User u  JOIN u.roles r WHERE r.name = 'CONSULTANT'    AND   u.deletedAt is NULL")
    List<User> findAllConsultants();

    @Query("SELECT distinct  u FROM User u  JOIN u.roles r WHERE r.name <> 'ADMIN_ACCOUNT' AND   u.deletedAt is NULL ORDER BY u.created_at DESC ")
    Page<User> findAllNonAdmins(Pageable pageable);

    @Query("SELECT distinct  u FROM User u  JOIN u.roles r WHERE r.name <> 'ADMIN_ACCOUNT' AND r.name <> 'STAFF' AND   u.deletedAt is NULL ORDER BY u.created_at DESC ")
    Page<User> findAllVendors(Pageable pageable);
    @Query("SELECT distinct  u FROM User u  JOIN u.roles r WHERE r.name <> 'ADMIN_ACCOUNT' AND r.name <> 'STAFF' AND   u.deletedAt is NULL")
    List<User> findAllVendors();


    @Query("SELECT q FROM  User q WHERE  q.userTypes IN (:userTypes) and q.deletedAt is NULL")
    public Page<User> findAllByRoles(@Param("userTypes") String[] userTypes, Pageable pageable);


    public User findOneByUsername(String username);

    public User findOneByUuid(String uuid);

    public User findOneByUsernameAndPassword(String username, String password);

    public User findByResetPasswordToken(String token);

    public User findByEmail(String email);

    @Modifying
    @Transactional
    @Query("update User u set u.lastLoggedIn = ?1 where u.username = ?2")
    public void setFixedLastLoggedIn(LocalDateTime lastLoggedIn, String username);


    @Query("SELECT u FROM User u WHERE  u.id IN(:userIds)")
    public List<User> findByUserIds(@Param("userIds") List<Long> userIds);
}

