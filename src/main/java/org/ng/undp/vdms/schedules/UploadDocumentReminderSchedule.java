package org.ng.undp.vdms.schedules;

import org.ng.undp.vdms.constants.SupplierDocumentType;
import org.ng.undp.vdms.domains.SupplierDocument;
import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.services.SmtpService;
import org.ng.undp.vdms.services.SupplierDocumentService;
import org.ng.undp.vdms.services.UserService;
import org.ng.undp.vdms.services.VendorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;

import java.util.List;

/**
 * Created by abdulhakim on 2/24/18.
 */
@Component
public class UploadDocumentReminderSchedule {

    private static Logger LOGGER = LoggerFactory.getLogger(UploadDocumentReminderSchedule.class);

    @Autowired
    private VendorService vendorService;

    @Autowired
    private UserService userService;

    @Autowired
    private SmtpService smtpService;

    @Autowired
    private SupplierDocumentService supplierDocumentService;


    @Scheduled(cron = "0 0 13 * * SUN",zone = "Africa/Lagos")
    public void supplierprofileUpdateNotificationReminder() {
        LOGGER.info("Background job for supplier reminder running");

        List<User> vendorList = userService.getAllSuppliers();

        for (User v : vendorList) {

            SupplierDocument registration = supplierDocumentService.findOneByUserAndSupplierDocumentType(v, SupplierDocumentType.REGISTRATION_CERTIFICATE);
            SupplierDocument financial = supplierDocumentService.findOneByUserAndSupplierDocumentType(v, SupplierDocumentType.FINANCIAL_REPORT);
            SupplierDocument directors = supplierDocumentService.findOneByUserAndSupplierDocumentType(v, SupplierDocumentType.DIRECTORS_FORM);

            if (null == registration || null == financial || null == directors) {
                LOGGER.info("EMailing Supplier : " + v.getUsername());
                sendVendorProfileUpdateNotificationReminder(v.getLastname(), v.getEmail());


            }

        }
    }


    @Async
    private void sendVendorProfileUpdateNotificationReminder(String lastname, String email) {
        // String homeURL = ServletUriComponentsBuilder.fromCurrentContextPath().path("/").build().toUriString();
        // String loginURL = ServletUriComponentsBuilder.fromCurrentContextPath().path("/accounts/login").build().toUriString();
        String SERVER_URL = "http://rosters.ng.undp.org";

        Context messageContext = new Context();
        messageContext.setVariable("userName", lastname);
        messageContext.setVariable("loginURL", SERVER_URL + "/accounts/login");
        messageContext.setVariable("homeURL", SERVER_URL);

        String messageBody = smtpService.prepareThymeleafMailBody("vendor-upload-reminder", messageContext);
        LOGGER.info("Background job send vendor upload reminder email");

        smtpService.sendSmtpAsync(email, " Upload your Company Registration , Financials Documents and FORM C07 ", messageBody, "", "");


    }
}
