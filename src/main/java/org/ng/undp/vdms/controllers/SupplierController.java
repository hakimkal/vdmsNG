package org.ng.undp.vdms.controllers;

import org.apache.log4j.Logger;
import org.ng.undp.vdms.constants.SupplierDocumentType;
import org.ng.undp.vdms.domains.SupplierCompany;
import org.ng.undp.vdms.domains.SupplierDocument;
import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.dto.*;
import org.ng.undp.vdms.services.*;
import org.ng.undp.vdms.storage.StorageService;
import org.ng.undp.vdms.utils.Auth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by abdulhakim on 11/12/16.
 */

@Controller
@PreAuthorize("hasAnyAuthority('ADMIN_ACCOUNT','SUPPLIER','STAFF','UNSTAFF')")

@RequestMapping(value = "suppliers")
public class SupplierController extends BaseController {

    private final Logger LOGGER = org.apache.log4j.Logger.getLogger(this.getClass());


    @Autowired
    private StorageService storageService;

    @Autowired
    private SupplierCompanyService companyService;
    @Autowired
    private SupplierFinancialService financialService;
    @Autowired
    private SupplierTechnicalService technicalService;
    @Autowired
    private SupplierExperienceService experienceService;
    @Autowired
    private SupplierOtherService otherService;
    @Autowired
    private SupplierProfileService profileService;
    @Autowired
    private UserService userService;
    @Autowired
    private SupplierDocumentService supplierDocumentService;


    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String addForm(Model model) {
        return "suppliers/add";
    }

    //======= Step 1

    @GetMapping(value = "/step-one")
    public ModelAndView stepOneView() {

        User AUTH_USER = Auth.INSTANCE.getAuth().get();

        SupplierCompanyDto companyDto =
                companyService.populateCompany(AUTH_USER);

        if (Objects.isNull(companyDto)) companyDto = new SupplierCompanyDto();


        ModelAndView mav =
                new ModelAndView("suppliers/step-one");
        mav.addObject("supplierCompanyDto", companyDto);
        return mav;
    }

    @PostMapping(value = "step-one")
    public ModelAndView stepOnePost(@Valid @ModelAttribute SupplierCompanyDto supplierCompanyDto, Errors errors) throws Exception {

        if (errors.hasErrors()) {

            return new ModelAndView("suppliers/step-one");
        }

        User AUTH_USER = Auth.INSTANCE.getAuth().get();

        ModelAndView mav =
                new ModelAndView("redirect:/suppliers/step-two");


           companyService.createSupplierCompany(supplierCompanyDto);



        if (!Objects.isNull(supplierCompanyDto.getSubsidiaries())) {
            if (supplierCompanyDto.getSubsidiaries().getSize() != 0) {
                MultipartFile subsidiaries = supplierCompanyDto.getSubsidiaries();
                LOGGER.info("There is a subsidiary file uploaded");
                LOGGER.debug(subsidiaries.toString());
                subsidiaries = supplierCompanyDto.getSubsidiaries();
                SupplierDocument supplierDocument = supplierDocumentService.findOneByUserAndSupplierDocumentType(AUTH_USER, SupplierDocumentType.getEnum("SUBSIDIARIES"));
                if (supplierDocument == null) {
                    supplierDocument = new SupplierDocument();
                } else {
                    LOGGER.info("SUPPLIER DOC IS NOT NULL");
                    storageService.delete(supplierDocument.getFileName());
                    LOGGER.info(supplierDocument.getFileName());

                }
                String fileName = prepareFile(subsidiaries);

                supplierDocument.setFileName(fileName);
                supplierDocument.setName(subsidiaries.getOriginalFilename());
                supplierDocument.setUser(AUTH_USER);
                supplierDocument.setSupplierDocumentType(SupplierDocumentType.getEnum("SUBSIDIARIES"));
                supplierDocumentService.save(supplierDocument);


            }
        }

        if (!Objects.isNull(supplierCompanyDto.getRegistration())) {
            if (supplierCompanyDto.getRegistration().getSize() != 0) {
                MultipartFile registration = supplierCompanyDto.getRegistration();
                LOGGER.info("There is a cac certificate file uploaded");
                LOGGER.debug(registration.toString());
                registration = supplierCompanyDto.getRegistration();
                SupplierDocument supplierDocument2 = supplierDocumentService.findOneByUserAndSupplierDocumentType(AUTH_USER, SupplierDocumentType.getEnum("REGISTRATION CERTIFICATE"));
                if (supplierDocument2 == null) {
                    supplierDocument2 = new SupplierDocument();
                } else {
                    LOGGER.info("SUPPLIER DOC 2 IS NOT NULL");
                    storageService.delete(supplierDocument2.getFileName());
                    LOGGER.info(supplierDocument2.getFileName());

                }
                String fileName = prepareFile(registration);

                supplierDocument2.setFileName(fileName);
                supplierDocument2.setName(registration.getOriginalFilename());
                supplierDocument2.setUser(AUTH_USER);
                supplierDocument2.setSupplierDocumentType(SupplierDocumentType.getEnum("REGISTRATION CERTIFICATE"));
                supplierDocumentService.save(supplierDocument2);


            }
        }

        if (!Objects.isNull(supplierCompanyDto.getDirectors())) {
            if (supplierCompanyDto.getDirectors().getSize() != 0) {
                MultipartFile registration = supplierCompanyDto.getDirectors();
                LOGGER.info("There is a cac form c07 file uploaded");
                LOGGER.debug(registration.toString());
                registration = supplierCompanyDto.getDirectors();
                SupplierDocument supplierDocument3 = supplierDocumentService.findOneByUserAndSupplierDocumentType(AUTH_USER, SupplierDocumentType.getEnum("FORM C07"));
                if (supplierDocument3 == null) {
                    supplierDocument3 = new SupplierDocument();
                } else {
                    LOGGER.info("SUPPLIER DOC 3 IS NOT NULL");
                    storageService.delete(supplierDocument3.getFileName());
                    LOGGER.info(supplierDocument3.getFileName());

                }
                String fileName = prepareFile(registration);

                supplierDocument3.setFileName(fileName);
                supplierDocument3.setName(registration.getOriginalFilename());
                supplierDocument3.setUser(AUTH_USER);
                supplierDocument3.setSupplierDocumentType(SupplierDocumentType.getEnum("FORM C07"));
                supplierDocumentService.save(supplierDocument3);


            }
        }
        return mav;
    }

    //======= Step 2
    @GetMapping(value = "/step-two")
    public ModelAndView stepTwoView() {

        User AUTH_USER = Auth.INSTANCE.getAuth().get();

        SupplierFinancialDto financialDto =
                financialService.populateFinancial(AUTH_USER);

        if (Objects.isNull(financialDto)) financialDto = new SupplierFinancialDto();

        ModelAndView mav =
                new ModelAndView("suppliers/step-two");
        mav.addObject("supplierFinancialDto", financialDto);
        return mav;
    }

    @PostMapping(value = "/step-two")
    public ModelAndView stepTwoPost(@Valid @ModelAttribute SupplierFinancialDto dto, Errors errors) throws Exception {


        if (errors.hasErrors()) {

            return new ModelAndView("suppliers/step-two");
        }

        User AUTH_USER = Auth.INSTANCE.getAuth().get();
        ModelAndView mav =
                new ModelAndView("redirect:/suppliers/step-three");

        financialService.createSupplierFinancial(dto);

        if (!Objects.isNull(dto.getSupplierDocument())) {

            if (dto.getSupplierDocument().getSize() != 0) {
                MultipartFile subsidiaries = dto.getSupplierDocument();
                LOGGER.info("There is a financial document file uploaded");
                LOGGER.debug(subsidiaries.toString());
                subsidiaries = dto.getSupplierDocument();
                SupplierDocument supplierDocument = supplierDocumentService.findOneByUserAndSupplierDocumentType(AUTH_USER, SupplierDocumentType.getEnum("FINANCIAL REPORT"));

                if (supplierDocument == null) {
                    supplierDocument = new SupplierDocument();
                } else {
                    LOGGER.info("SUPPLIER FINANCIAL DOC IS NOT NULL");
                    storageService.delete(supplierDocument.getFileName());
                    LOGGER.info(supplierDocument.getFileName());
                }
                String fileName = prepareFile(subsidiaries);

                supplierDocument.setFileName(fileName);
                supplierDocument.setName(subsidiaries.getOriginalFilename());
                supplierDocument.setUser(AUTH_USER);
                supplierDocument.setSupplierDocumentType(SupplierDocumentType.getEnum("FINANCIAL REPORT"));
                supplierDocumentService.save(supplierDocument);


            }
        }


        return mav;
    }

    //======= Step 3
    @GetMapping(value = "/step-three")
    public ModelAndView stepThreeView() {

        User AUTH_USER = Auth.INSTANCE.getAuth().get();

        SupplierTechnicalDto technicalDto =
                technicalService.populateTechnical(AUTH_USER);

        if (Objects.isNull(technicalDto)) technicalDto = new SupplierTechnicalDto();

        ModelAndView mav =
                new ModelAndView("suppliers/step-three");
        mav.addObject("supplierTechnicalDto", technicalDto);
        return mav;
    }

    @PostMapping(value = "/step-three")
    public ModelAndView stepThreePost(@Valid @ModelAttribute SupplierTechnicalDto dto, Errors errors) throws Exception {


        if (errors.hasErrors()) {

            return new ModelAndView("suppliers/step-three");
        }

        User AUTH_USER = Auth.INSTANCE.getAuth().get();
        ModelAndView mav =
                new ModelAndView("redirect:/suppliers/step-four");

        technicalService.createTechnical(dto);

        if (!Objects.isNull(dto.getSupplierDocument())) {

            if (dto.getSupplierDocument().getSize() != 0) {
                MultipartFile subsidiaries = dto.getSupplierDocument();
                LOGGER.info("There is a TECHNICAL document file uploaded");
                LOGGER.debug(subsidiaries.toString());
                subsidiaries = dto.getSupplierDocument();
                SupplierDocument supplierDocument = supplierDocumentService.findOneByUserAndSupplierDocumentType(AUTH_USER, SupplierDocumentType.getEnum("QUALITY ASSURANCE CERTIFICATION"));

                if (supplierDocument == null) {
                    supplierDocument = new SupplierDocument();
                } else {
                    LOGGER.info("SUPPLIER TECHNICAL DOC IS NOT NULL");
                    storageService.delete(supplierDocument.getFileName());
                    LOGGER.info(supplierDocument.getFileName());
                }
                String fileName = prepareFile(subsidiaries);

                supplierDocument.setFileName(fileName);
                supplierDocument.setName(subsidiaries.getOriginalFilename());
                supplierDocument.setUser(AUTH_USER);
                supplierDocument.setSupplierDocumentType(SupplierDocumentType.getEnum("QUALITY ASSURANCE CERTIFICATION"));
                supplierDocumentService.save(supplierDocument);


            }
        }


        return mav;
    }

    //======= Step 4
    @GetMapping(value = "/step-four")
    public ModelAndView stepFourView() {
        User AUTH_USER = Auth.INSTANCE.getAuth().get();

        SupplierExperienceDto experienceDto =
                experienceService.populateExperience(AUTH_USER);

        if (Objects.isNull(experienceDto)) experienceDto = new SupplierExperienceDto();

        ModelAndView mav =
                new ModelAndView("suppliers/step-four");
        mav.addObject("supplierExperienceDto", experienceDto);
        return mav;
    }

    @PostMapping(value = "/step-four")
    public ModelAndView stepFourPost(@Valid @ModelAttribute SupplierExperienceDto dto, Errors errors) throws Exception {


        if (errors.hasErrors()) {

            return new ModelAndView("suppliers/step-four");
        }

        User AUTH_USER = Auth.INSTANCE.getAuth().get();
        ModelAndView mav =
                new ModelAndView("redirect:/suppliers/step-five");

        experienceService.createSupplierExperience(dto);

        if (!Objects.isNull(dto.getSupplierDocument())) {

            if (dto.getSupplierDocument().getSize() != 0) {
                MultipartFile subsidiaries = dto.getSupplierDocument();
                LOGGER.info("There is a EXPERIENCE document file uploaded");
                LOGGER.debug(subsidiaries.toString());
                subsidiaries = dto.getSupplierDocument();
                SupplierDocument supplierDocument = supplierDocumentService.findOneByUserAndSupplierDocumentType(AUTH_USER, SupplierDocumentType.getEnum("ENVIRONMENTAL POLICY"));

                if (supplierDocument == null) {
                    supplierDocument = new SupplierDocument();
                } else {
                    LOGGER.info("SUPPLIER EXPERIENCE DOC IS NOT NULL");
                    storageService.delete(supplierDocument.getFileName());
                    LOGGER.info(supplierDocument.getFileName());
                }
                String fileName = prepareFile(subsidiaries);

                supplierDocument.setFileName(fileName);
                supplierDocument.setName(subsidiaries.getOriginalFilename());
                supplierDocument.setUser(AUTH_USER);
                supplierDocument.setSupplierDocumentType(SupplierDocumentType.getEnum("ENVIRONMENTAL POLICY"));
                supplierDocumentService.save(supplierDocument);


            }
        }

        return mav;
    }

    //======= Step 5
    @GetMapping(value = "/step-five")
    public ModelAndView stepFiveView() {

        User AUTH_USER = Auth.INSTANCE.getAuth().get();

        SupplierOtherDto otherDto =
                otherService.populateOther(AUTH_USER);

        if (Objects.isNull(otherDto)) otherDto = new SupplierOtherDto();


        ModelAndView mav =
                new ModelAndView("suppliers/step-five");
        mav.addObject("supplierOtherDto", otherDto);
        return mav;
    }

    @PostMapping(value = "/step-five")
    public ModelAndView stepFivePost(@Valid @ModelAttribute SupplierOtherDto dto, Errors errors) {


        if (errors.hasErrors()) {

            return new ModelAndView("suppliers/step-five");
        }

        ModelAndView mav =
                new ModelAndView("redirect:/suppliers/confirm");

        otherService.createSupplierOther(dto);

        return mav;
    }

    //======= Confirm
    @GetMapping(value = "/confirm")
    public ModelAndView confirmView() {


        User AUTH_USER = Auth.INSTANCE.getAuth().get();

        SupplierProfileDto profileDto =
                profileService.populateSupplierDto(AUTH_USER);

        if (Objects.isNull(profileDto)) profileDto = new SupplierProfileDto();
        ModelAndView mav =
                new ModelAndView("suppliers/confirm");


        mav.addObject("profileDto", profileDto);

        return mav;
    }


    @PostMapping(value = "/confirm")
    public ModelAndView confirmPost() {


        ModelAndView mav =
                new ModelAndView("redirect:/suppliers");
        return mav;
    }


    //======= PREVIEW
    @GetMapping(value = "profile/{id}")
    public ModelAndView profileView(@PathVariable Long id) {


        User AUTH_USER = userService.getUserById(id);

        SupplierProfileDto profileDto =
                profileService.populateSupplierDto(AUTH_USER);

        List<SupplierDocument> supplierDocuments = supplierDocumentService.findAllByUser(AUTH_USER);


        if (Objects.isNull(profileDto)) profileDto = new SupplierProfileDto();
        ModelAndView mav =
                new ModelAndView("suppliers/confirm");

        mav.addObject("profileDto", profileDto);
        mav.addObject("supplierDocs", supplierDocuments);


        return mav;
    }


    private String prepareFile(MultipartFile file) throws IOException, Exception {
        if (file == null || file.isEmpty()) {
            LOGGER.info("Empty file.");
            //return getErrorMap("Error", "File is empty");
            throw new Exception("Submitted file should not be empty");
        }


        String fileName = UUID.randomUUID().toString();
        return storageService.store(file, fileName);
    }
}
