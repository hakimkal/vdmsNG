package org.ng.undp.vdms.domains;

import org.ng.undp.vdms.dao.Accessor;
import org.ng.undp.vdms.dao.Filter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by abdulhakim on 7/16/17.
 */
@Table
@Entity

public class VendorVpa {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vendor_id")


    private Vendor vendor;

    private Date created_at;
    private Date updated_at;

    /*
     * This field is here to check whether a field is deleted or not.
     * by default, the field is NULL, if it carries a timestamp instead of null
     * then the record has been deleted,
     */
    private Date deleted_at;

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public Date getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(Date deleted_at) {
        this.deleted_at = deleted_at;
    }

    public Vpa getVpa() {
        return vpa;
    }

    public void setVpa(Vpa vpa) {
        this.vpa = vpa;
    }

    @ManyToOne
    private Vpa vpa;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }


    @PreUpdate
    void updatedAt() {
        this.updated_at = new Date();
    }


    @PrePersist
    void createAt() {


        this.created_at = this.updated_at = new Date();


    }

}
