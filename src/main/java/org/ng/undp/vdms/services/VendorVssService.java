package org.ng.undp.vdms.services;


import org.ng.undp.vdms.domains.Vendor;
import org.ng.undp.vdms.domains.VendorVss;
import org.ng.undp.vdms.domains.Vss;
import org.ng.undp.vdms.repositories.VendorVssRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by abdulhakim on 7/16/17.
 */
@Service
public class VendorVssService {

    @Autowired
    private VendorVssRepository vendorVssRepository;

    public Iterable<VendorVss> findAll() {

        return vendorVssRepository.findAll();
    }



    public VendorVss getVpa(Long id) {
        return vendorVssRepository.findOne(id);
    }
    public VendorVss findOneByVendorAndVss(Vendor v, Vss id) {
        return vendorVssRepository.findOneByVendorAndVss(v,id);
    }

    public VendorVss editVpa(VendorVss vpa) {
        return vendorVssRepository.save(vpa);
    }

    public void delete(Long id) {
        vendorVssRepository.delete(id);
    }
    public VendorVss save(VendorVss d) {
        return vendorVssRepository.save(d);
    }

    public List<VendorVss> findAllById(List<Long> vpaIds){return  vendorVssRepository.findAllById(vpaIds);}


}
