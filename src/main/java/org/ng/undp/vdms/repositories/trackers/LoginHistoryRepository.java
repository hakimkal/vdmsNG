package org.ng.undp.vdms.repositories.trackers;


import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.domains.trackers.LoginHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by emmanuel on 12/16/16.
 */
public interface LoginHistoryRepository extends JpaRepository<LoginHistory, Long> {
    public Page<LoginHistory> findAllByOrderByLoginTimeDesc(Pageable pageable);
    public Page<LoginHistory> findAllByUserOrderByLoginTimeDesc(Pageable pageable,User user);

}
