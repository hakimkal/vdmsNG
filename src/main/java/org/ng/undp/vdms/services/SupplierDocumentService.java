package org.ng.undp.vdms.services;

import org.ng.undp.vdms.constants.SupplierDocumentType;
import org.ng.undp.vdms.domains.SupplierDocument;
import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.repositories.SupplierDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: Kingsley Eze.
 * Project: vdms
 * Date: 6/19/2017.
 */

@Service
public class SupplierDocumentService {

    @Autowired
    private SupplierDocumentRepository repository;

    public SupplierDocument save(SupplierDocument document){
        return repository.save(document);
    }
    public SupplierDocument findOne(Long document){
        return repository.findOne(document);
    }

    public SupplierDocument findOneByUserAndSupplierDocumentType(User user, SupplierDocumentType supplierDocumentType){
        return repository.findOneByUserAndSupplierDocumentType(user,supplierDocumentType);
    }

    public List<SupplierDocument> findAllByUser(User user){
        return repository.findAllByUser(user);
    }
}
