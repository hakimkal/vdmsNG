package org.ng.undp.vdms.schedules;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.ng.undp.vdms.domains.Contract;
import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.domains.constants.Status;
import org.ng.undp.vdms.services.ContractService;
import org.ng.undp.vdms.services.SmtpService;
import org.ng.undp.vdms.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by macbook on 6/30/17.
 */

@Component
public class ContractReminderSchedule {
    @Autowired
    ContractService contractService;
    @Autowired
    private SmtpService smtpService;

    @Autowired
    private UserService userService;
    private int REMAINING_DAYS = 14;

    private Date TODAY;

    private static final Logger LOGGER = org.apache.log4j.Logger.getLogger(ContractReminderSchedule.class);

    public ContractReminderSchedule() {

        //  TODAY = new Date();
    }

    /**
     * Checks the days remaining before terminating user subscription.
     */
    // @Scheduled(fixedDelay = 60000)
    @Scheduled(cron = "0 30 9 * * *", zone = "Africa/Lagos")
    public void subscriptionRemainingDaysChecker() {

        System.out.println("Checks the days remaining before terminating user subscription");
        Iterable<Contract> subscriptions = contractService.findAll();

        if (!Objects.isNull(subscriptions)) {

            for (Contract subscription : subscriptions) {
                TODAY = new Date();
                try {

                    DateTime dateTimeToday = new DateTime(TODAY);
                    DateTime dateTimeToExpire =
                            new DateTime(subscription.getContractExpiryDate());


                    int SubscriptionDaysRemains = Days.daysBetween(dateTimeToday, dateTimeToExpire).getDays();
                    if (SubscriptionDaysRemains >=0 && SubscriptionDaysRemains <= REMAINING_DAYS && Objects.isNull(subscription.getStatus())) {
                        updateContract(subscription, SubscriptionDaysRemains);
                    }

                    if (SubscriptionDaysRemains <= REMAINING_DAYS && !Objects.isNull(subscription.getStatus()) && !subscription.getStatus().equals(Status.COMPLETED)) {
                        System.out.println("INCOMPLETE CONTRACT BATCH");
                        updateContract(subscription, SubscriptionDaysRemains);

                    }


                } catch (Exception e) {

                    LOGGER.warn("Error occurred while  parsing contract : " + subscription, e);

                }
            }
        }
    }


    public void updateContract(Contract contract, int subscriptionDaysRemains) {

        contract.setReminderSent(true);
        contract.setReminderSentCount(contract.getReminderSentCount() + 1);
        contract.setEmailSentDate(TODAY);
        contractService.save(contract);
        List<User> userList = userService.getAllAdmin();
        contractExpiryNotificationMail(userList, subscriptionDaysRemains, contract);

    }

    public void contractExpiryNotificationMail(User user, int remainingDays, Contract contract) {


        sendContractReminder(user, remainingDays, contract);


    }


    public void contractExpiryNotificationMail(List<User> users, int remainingDays, Contract contract) {

        for (User user : users) {
            sendContractReminder(user, remainingDays, contract);
        }
    }


    private void sendContractReminder(User user, int remainingDays, Contract contract) {
        final Context messageContext = new Context();
        messageContext.setVariable("userName", user.getLastname());
        messageContext.setVariable("remainingDays", remainingDays);
        messageContext.setVariable("vendorName", contract.getVendor().getVendorName());
        messageContext.setVariable("contractTitle", contract.getName());
        messageContext.setVariable("expiryDate", contract.getContractExpiryDate());
        String redirectionUrl = ("http://rosters.ng.undp.org");

        messageContext.setVariable("homeURL", redirectionUrl);


        String messageBody =
                smtpService.prepareThymeleafMailBody("contract-reminder", messageContext);

        smtpService.sendSmtpAsync(user.getEmail(), "Contract: " + contract.getName() + " expires next " + remainingDays + " days", messageBody, "", "");

    }

}
