package org.ng.undp.vdms.services;


import org.ng.undp.vdms.configs.NetWorkUtil;
import org.ng.undp.vdms.dao.Accessor;
import org.ng.undp.vdms.dao.Filter;
import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.domains.constants.Env;
import org.ng.undp.vdms.domains.trackers.LoginHistory;
import org.ng.undp.vdms.repositories.trackers.LoginHistoryRepository;
import org.ng.undp.vdms.utils.Auth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by emmanuel on 7/12/17.
 */
@Service
public class LoginHistoryService {

    @Autowired
    private LoginHistoryRepository loginHistoryRepository;

    @Autowired
    private UtilsService utilsService;
    @Autowired
    private UserService userService;

    public void save(LoginHistory loginHistory) {
        if (loginHistory != null) {
            loginHistoryRepository.save(loginHistory);
        }
    }

    public LoginHistory findHistoryById(Long id) {
        return loginHistoryRepository.findOne(id);
    }


    public Optional<LoginHistory> findLastHistoryForUser(User user) {
        if (user == null || user.getId() == null) {
            return Optional.ofNullable(null);
        }

        return Accessor.findOne(LoginHistory.class, Filter.get().field("user.id", user.getId())
                .field("env", utilsService.getEnvironmentProfile()), "loginTime desc");
    }

    public Page<LoginHistory> findAll(Pageable pageable) {
        return loginHistoryRepository.findAllByOrderByLoginTimeDesc(pageable);
    }

    public Page<LoginHistory> findAllByUser(Pageable pageable,User user) {
        return loginHistoryRepository.findAllByUserOrderByLoginTimeDesc(pageable ,user);
    }

    public void createNewLoginRecord(HttpServletRequest request) throws Exception {
        Optional<User> optional = Auth.INSTANCE.getAuth();
        if (!optional.isPresent()) {
            return;
        }

        LoginHistory loginHistory = new LoginHistory();
        loginHistory.setLoginTime(LocalDateTime.now());
        loginHistory.setEnv(utilsService.getEnvironmentProfile());
        loginHistory.setIp(NetWorkUtil.getIp());
        loginHistory.setPcName(request.getRemoteHost());
        loginHistory.setMacAddress(request.getRemoteHost());
        loginHistory.setOs(NetWorkUtil.getOsName());

        loginHistory.setUser(optional.get());
        loginHistory.setUuid(UUID.randomUUID().toString());

        loginHistoryRepository.save(loginHistory);
    }


    public void updateLogoutTime(String userName) {

        System.out.println("The username intercepted from logout " + userName);
        Optional<LoginHistory> optional = Accessor.findOne(LoginHistory.class, Filter.get()
                        .field("user.username", userName)
                        .field("env", utilsService.getEnvironmentProfile())
                , "loginTime desc");

        if (optional.isPresent()) {
            LoginHistory history = optional.get();
            history.setLogoutTime(LocalDateTime.now());
            loginHistoryRepository.save(history);
        }
    }

    public void updateLastLogoutTime(HttpServletRequest request) throws Exception {
        String ip;
        if (userService.getEnvironmentProfile() == Env.DEV) {
            ip = "127.0.0.1";
        } else {
            ip = NetWorkUtil.getIp();
        }


        Optional<LoginHistory> optional = Accessor.findOne(LoginHistory.class, Filter.get()
                        .field("ip", ip)
                        .field("env", utilsService.getEnvironmentProfile())
                , "loginTime desc");

        if (optional.isPresent()) {
            LoginHistory history = optional.get();
            history.setLogoutTime(LocalDateTime.now());
            loginHistoryRepository.save(history);
        }
    }
}
